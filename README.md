# Vim
For VIM config files.

## Install Instructions
1. Copy .vimrc to ~/.vimrc
2. Copy .vim to ~/.vim
3. Install Vundle (https://github.com/VundleVim/Vundle.vim)
4. Open vim and run `:PluginInstall`
5. Build YouCompleteMe (https://github.com/ycm-core/YouCompleteMe)
